## ENTREGABLE_01 

## 1.Consulta DEFAULT:
Consultamos la fecha de contrato de un empleado;
*SOLUCION:
```sql
 SELECT numero_empleado,nombre, apellido, Fecha_Alta
 FROM Empleados;
 ```
 | Numero_empleado | Nombre | Apellido | Fecha_alta| 
 |-----------:| -----------:|
 | 1  | Juan  | Perez| 2020-01-01 | 
 | 2  | Maria | Lopez| 2019-03-15 | 

## 2.CONSULTA en UNIQUE
Listaremos todos los nombres de los departamentos.
*SOLUCION:
```sql
select id_departamento as id, Nombre_Departamento as ND, Descripcion_Departamento as DD
from departamento;
```
| id | Nombre_Departamento | Descripcion_Departamento |
 |-------:| -------:|-------:|
 | 1  | 1 | D_143, salud| 
 | 2  | 2 | D_123, salud| 

## 3.Consulta con CHECH:
Listamos al empleado del departamento con ID 1:
*SOLUCION:
```sql
select numero_empleado, nombre, apellido, departamento
From Empleados
Where departamento = 1;
```
## 4.Consulta con IDENTITY:
Listamos los nombres y apellidos de los pacientes con sus ID;
*SOLUCION:
```sql
select numero_paciente, nombre, apellido
from Pacientes;

```
| numero_paciente | Nombre | Apellido |
 |-------:| -------:|-------:|
 | 1  | Carlos | Gonzalez | 
 | 2  | Ana    | Martinez |


## base de datos Clinica "Buena Salud":
# TABLAS EMPLEADO, PACIENTE, DEPARTAMENTO:

# CREATE DATABASE IF NOT EXISTS ClinicaDB;
# USE ClinicaDB;

``` sql
-- Tabla de empleados
CREATE TABLE Empleados (
                           Numero_Empleado INT PRIMARY KEY,
                           Nombre VARCHAR(100),
                           Apellido VARCHAR(100),
                           Fecha_Alta DATE,
                           Departamento VARCHAR(100),
                           Oficio VARCHAR(100),
                           Salario DECIMAL(10, 2),
                           Edad INT
);

-- Tabla de pacientes
CREATE TABLE Pacientes (
                           Numero_Paciente INT PRIMARY KEY,
                           Nombre VARCHAR(100),
                           Apellido VARCHAR(100),
                           Fecha_Nacimiento DATE,
                           Genero VARCHAR(10),
                           Telefono VARCHAR(20),
                           Direccion VARCHAR(255)
);

-- Tabla de consultas
CREATE TABLE Consultas (
                           Numero_Consulta INT PRIMARY KEY,
                           Numero_Paciente INT,
                           Numero_Empleado INT,
                           Fecha_Consulta DATE,
                           Especialidad VARCHAR(100),
                           Diagnostico VARCHAR(255),
                           Tratamiento VARCHAR(255),
                           FOREIGN KEY (Numero_Paciente) REFERENCES Pacientes(Numero_Paciente),
                           FOREIGN KEY (Numero_Empleado) REFERENCES Empleados(Numero_Empleado)
);

-- tablas departamento
CREATE TABLE departamento(
    id_departamento INT PRIMARY KEY AUTO_INCREMENT,
    Nombre_Departamento VARCHAR(100) not null,
    Descripcion_Departamento text
);

INSERT INTO Empleados (Numero_Empleado, Nombre, Apellido, Fecha_Alta, Departamento, Oficio, Salario, Edad)
VALUES (1, 'Juan', 'Peraza', '2020-01-01', 'Medicina General', 'Médico', 5000.00, 35),
       (2, 'Maria', 'Lopez', '2019-03-15', 'Pediatría', 'Pediatra', 4800.00, 40);

INSERT INTO Pacientes (Numero_Paciente, Nombre, Apellido, Fecha_Nacimiento, Genero, Telefono, Direccion)
VALUES (1, 'Camilo', 'Gonzalez', '1980-05-10', 'Masculino', '555-1234', 'Calle Principal 123'),
       (2, 'Analiz', 'Martines', '1995-09-20', 'Femenino', '555-5678', 'Avenida Central 456');

INSERT INTO Consultas (Numero_consulta, Numero_Paciente, Numero_Empleado, Fecha_Consulta, Especialidad, Diagnostico, Tratamiento)
VALUES (1, 1, 1, '2021-06-01', 'Medicina General', 'Resfriado común', 'Recetar medicamentos y reposo'),
       (2, 2, 2, '2021-06-05', 'Pediatría', 'Control de crecimiento', 'Revisión y seguimiento pediátrico');

INSERT INTO Departamento(nombre_departamento, descripcion_departamento)
VALUES (1,'D_143, salud' ),
       (2,'D_123, salud' );

```


## EJECUTAR OPERACIONES BASICAS TIPO DCL Y TCL PARA CREAR PROCEIMIENTOS Y FUNCIONES:

```sql
-- DCL
DELIMITER //
CREATE PROCEDURE InsertarDeparamento(
    IN p_nombre_departamento varchar (100),
    IN p_descripcion_departamento TEXT
)
    BEGIN
        DECLARE EXIT HANDLER FOR SQLEXCEPTION
            BEGIN
                ROLLBACK ;
                select 'error: no se pudo insertar el registro';
            end;
        start TRANSACTION ;
        INSERT INTO Clinica.departamento (nombre_departamento, descripcion_deparamento)
        VALUES (p_nombre_departamento, p_descripcion_departamento);
        COMMIT;
        SELECT 'Registro insertar correcamente';
        END //
        DELIMITER ;


-- TCL

DELIMITER //
CREATE PROCEDURE InsertarDepartamentoConError(
    IN p_nombre_departamento varchar (100),
    IN p_descripcion_departamento TEXT
)
    BEGIN
        DECLARE EXIT HANDLER FOR SQLEXCEPTION
            BEGIN
                ROLLBACK ;
                select 'error: no se pudo insertar el registro';
            end;
        start TRANSACTION ;
        INSERT INTO Clinica.departamento (nombre_departamento, descripcion_deparamento)
        VALUES (p_nombre_departamento, p_descripcion_departamento);

        DECLARE error_condition BOOLEAN DEFAULT FALSE;
        If error_condition THEN
            SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Simulacion de error';
        end IF;
COMMIT;
SELECT 'Registro insertado correctamente';
    END //
DELIMITER ;

```